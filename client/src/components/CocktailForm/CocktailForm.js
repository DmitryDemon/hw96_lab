import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

class CocktailForm extends Component {
    _createIngredientItem = () => {
        return {title:'',amount:'', key: Math.random().toString()}
    };
    state = {
        title: '',
        recipe:'',
        image:'',
        ingredient: [
            this._createIngredientItem()
        ],
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            if (key === 'ingredient') {
                formData.append(key, JSON.stringify(this.state[key]));
            } else
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    addIngredient = event => {
        event.preventDefault();
        this.setState({
            ingredient:[
                ...this.state.ingredient,
                this._createIngredientItem()
            ]
        })
    };

    removeIngredient = idx =>{
        const ingredient = [...this.state.ingredient];
        ingredient.splice(idx,1);
        this.setState({ingredient})
    };

    ingredientInputChengeHandler = (event, idx) => {
        const oneIngredient = {...this.state.ingredient[idx]};

        oneIngredient[event.target.name] = event.target.value;

        const ingredient = [...this.state.ingredient];
        ingredient[idx] = oneIngredient;

        this.setState({ingredient})
    };

    render() {
        console.log(this.state);
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text" required
                    onChange={this.inputChangeHandler}
                    value={this.state.title}
                />

                <FormElement
                    propertyName="recipe"
                    title="Recipe"
                    type="textarea" required
                    value={this.props.recipe}
                    onChange={this.inputChangeHandler}
                />

                {this.state.ingredient.map((ing, idx) => (
                  <FormGroup key={ing.key} row>
                    <Label for="ing" sm={2}>Ingredient:</Label>
                      <Col sm={4}>
                        <Input
                          id='ing'
                          type='text'
                          name='title'
                          placeholder="Ingredient name"
                          onChange={(event) => this.ingredientInputChengeHandler(event,idx)}
                        />
                      </Col>
                      <Col sm={4}>
                        <Input
                          id='ing'
                          type='text'
                          name='amount'
                          placeholder="Amount"
                          onChange={(event) => this.ingredientInputChengeHandler(event,idx)}
                        />
                      </Col>
                      {idx > 0 && <Button onClick={() => this.removeIngredient(idx)}>X</Button>}
                  </FormGroup>
                ))}

                <FormGroup>
                  <Col sm={{offset:2, size: 10}}>
                    <Button onClick={this.addIngredient}>Add ingredients</Button>
                  </Col>
                </FormGroup>

                <FormElement
                    propertyName="image"
                    title="Cocktail Image"
                    type="file" required
                    onChange={this.fileChangeHandler}
                />

                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Create cocktail</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default CocktailForm;