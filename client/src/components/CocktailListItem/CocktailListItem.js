import React from 'react';
import PropTypes from 'prop-types';
import CocktailThumbnail from "../CocktailThumbnail/CocktailThumbnail";
import './CocktailListItem.css';
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";

const CocktailListItem = props => {
  return (
    <div className='CardBody'>
      <div className='Card'>
        <div className='Img'>
          <Link to={`/posts/${props._id}`}>
            <h2>{props.title}</h2>
          </Link>
          <CocktailThumbnail image={props.image}/>
        </div>
        <div className='Ing'>
          <p>Ingredients:</p>
          {props.ingredient.map((obj,key) => {
            return <ul key={key}>
                       <li>{obj.title} {obj.amount}</li>
                    </ul>
          })}
        </div>
        <div className='Recipe'>
          <b>Recipe:</b>
          <p>
            {props.recipe}
          </p>
        </div>
      </div>
      <div>OneCoctail
        <Row>
          {props.user && props.user.role === 'admin' &&
          <Col sm={2}>
            <Button
              style={{margin: '20px'}}
              color="primary"
              className="float-left"
              onClick={() => props.publishCocktail(props._id, {published: true})}
            >
              Publish
            </Button>
          </Col>
          }
          <Col sm={4}>
            {(props.published === false) ? <h5 style={{color: 'red', textAlign: 'center'}}>
              <strong>Ваш коктейль находится на рассмотрении модератора!</strong>
            </h5>: null}
          </Col>
          <Col sm={4}>
            {(props.removed === true) ? <h5 style={{color: 'red', textAlign: 'center'}}>
              <strong>Удалено!</strong>
            </h5>: null}
          </Col>
          {props.user && props.user.role === 'admin' &&
          <Col sm={2}>
            <Button
              style={{margin: '20px'}}
              color="danger"
              className="float-right"
              onClick={() => props.removeCocktail(props._id, {removed: true})}
            >
              Remove
            </Button>
          </Col>
          }
        </Row>
    </div>
    </div>
  );
};

CocktailListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  recipe: PropTypes.string.isRequired,
  ingredient: PropTypes.array.isRequired,
};

export default CocktailListItem;
