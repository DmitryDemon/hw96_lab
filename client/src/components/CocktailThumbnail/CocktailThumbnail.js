import React from 'react';

import imageNotAvailable from '../../assets/images/image_not_available.png';
import {apiURL} from "../../constants";

const styles = {
  width: '100%',
  height: '70%',
  marginRight: '10px',
  marginBottom: '20px'
};

const CocktailThumbnail = props => {
  let image = imageNotAvailable;

  if (props.image) {
    image = apiURL + '/uploads/' + props.image;
  }

  return <img src={image} style={styles} className="img-thumbnail" alt="Product" />;
};

export default CocktailThumbnail;
