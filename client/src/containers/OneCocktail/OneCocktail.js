import React, {Component, Fragment} from 'react';
// import {Card, CardBody, CardImg, CardText, CardTitle} from "reactstrap";
// import {apiURL} from "../../constants";
import {connect} from "react-redux";
import {fetchOneCocktail} from "../../store/actions/oneCocktailActions";
import {Link} from "react-router-dom";
import CocktailThumbnail from "../../components/CocktailThumbnail/CocktailThumbnail";
import '../../components/CocktailListItem/CocktailListItem.css';


class oneCocktail extends Component {

    componentDidMount() {
        this.props.onFetchCocktail(this.props.match.params.id);
    }

    render() {
        console.log(this.props.oneCocktail);
        return (
            <Fragment>
              {this.props.oneCocktail ?
                <div className='CardBody'>
                  <div className='Card'>
                    <div className='Img'>
                      <h2>{this.props.oneCocktail.cocktail.title}</h2>
                      <CocktailThumbnail image={this.props.oneCocktail.cocktail.image}/>
                    </div>
                    <div className='Ing'>
                      <p>Ingredients:</p>
                      {this.props.oneCocktail.cocktail.ingredient.map((obj,key) => {
                        return <ul key={key}>
                          <li>{obj.title} {obj.amount}</li>
                        </ul>
                      })}
                    </div>
                    <div className='Recipe'>
                      <b>Recipe:</b>
                      <p>
                        {this.props.oneCocktail.cocktail.recipe}
                      </p>
                    </div>
                  </div>
                </div>: null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    oneCocktail: state.oneCocktail.oneCocktail,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchCocktail: (cocktails) => dispatch(fetchOneCocktail(cocktails)),
});

export default connect(mapStateToProps, mapDispatchToProps)(oneCocktail);
