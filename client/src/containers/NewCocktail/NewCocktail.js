import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {createCocktail} from "../../store/actions/cocktailsActions";


class NewCocktail extends Component {

  createCocktail = cocktailData => {
    this.props.onCocktailCreated(cocktailData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <h2>New product</h2>
        <CocktailForm
          onSubmit={this.createCocktail}
          categories={this.props.categories}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails.cocktails,
});

const mapDispatchToProps = dispatch => ({
  onCocktailCreated: cocktailData => dispatch(createCocktail(cocktailData)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewCocktail);
