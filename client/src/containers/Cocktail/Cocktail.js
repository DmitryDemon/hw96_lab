import React, {Component} from 'react';
import {Button, Col, Row} from "reactstrap";
import {connect} from "react-redux";

import CocktailListItem from "../../components/CocktailListItem/CocktailListItem";
import {fetchCocktails, publishCocktail, removedCocktail} from "../../store/actions/cocktailsActions";
import {Link} from "react-router-dom";

class Cocktails extends Component {
    componentDidMount() {
        this.props.fetchCocktails();
    }

    componentDidUpdate() {
      if (!this.props.user) this.props.fetchCocktails();
    };

    render() {
        console.log(this.props.cocktails);
        return (
            <Row>
                <Col sm={12}>
                    <h2>
                      Cocktails
                      {(this.props.user && this.props.user.role === 'admin') || (this.props.user && this.props.user.role === 'user') ?
                      <Link to="/cocktails/new">
                          <Button
                              color="primary"
                              className="float-right"
                          >
                              Add product
                          </Button>
                      </Link>
                      : null}
                    </h2>
                </Col>
                <Col sm={12}>
                  {this.props.cocktails.map(cocktail => (
                    <CocktailListItem
                      key={cocktail._id}
                      _id={cocktail._id}
                      title={cocktail.title}
                      recipe={cocktail.recipe}
                      image={cocktail.image}
                      ingredient={cocktail.ingredient}
                      published={cocktail.published}
                      removed={cocktail.removed}
                      user={this.props.user}
                      publishCocktail={this.props.publishCocktail}
                      removeCocktail={this.props.removeCocktail}
                    />
                    ))}
                </Col>
            </Row>
        )
    }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails.cocktails,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchCocktails: () => dispatch(fetchCocktails()),
  removeCocktail: (id, remove) => dispatch(removedCocktail(id, remove)),
  publishCocktail: (id, published) => dispatch(publishCocktail(id, published))

});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);
