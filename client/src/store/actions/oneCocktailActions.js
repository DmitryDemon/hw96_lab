import axios from '../../axios-api';

export const FETCH_ONE_COCKTAIL_SUCCESS = 'FETCH_ONE_COCKTAIL_SUCCESS';

export const fetchOneCocktailSuccess = coctails => ({type: FETCH_ONE_COCKTAIL_SUCCESS, coctails});

export const fetchOneCocktail = id => {
  return (dispatch) => {
    return axios.get('/cocktails/' + id).then(
      response => {
        dispatch(fetchOneCocktailSuccess(response.data));
      }
    );
  };
};