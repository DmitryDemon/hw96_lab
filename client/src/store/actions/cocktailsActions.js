import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const CREATE_COCKTAIL_SUCCESS = 'CREATE_COCKTAIL_SUCCESS';

export const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAIL_SUCCESS, cocktails});
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});

export const fetchCocktails = () => {
  return dispatch => {
    return axios.get('/cocktails').then(
      response => dispatch(fetchCocktailsSuccess(response.data))
    );
  };
};

export const createCocktail = cocktailData => {
  return dispatch => {
    return axios.post('/cocktails', cocktailData).then(
      () => dispatch(createCocktailSuccess())
    );
  };
};

export const publishCocktail = (id, publish) => {
  return dispatch => {
    return axios.post(`/cocktails/${id}/toggle_published`, publish).then(
      () => {
        dispatch(fetchCocktails());
        NotificationManager.success('You published an cocktail!');
      }
    );
  };
};

export const removedCocktail = (id, remove) => {
  return dispatch => {
    return axios.post(`/cocktails/${id}/toggle_removed`, remove).then(
      () => {
        dispatch(fetchCocktails());
        NotificationManager.warning('You removed an cocktail!');
      }
    );
  };
};
