import {FETCH_COCKTAIL_SUCCESS} from "../actions/cocktailsActions";
import {FETCH_ONE_COCKTAIL_SUCCESS} from "../actions/oneCocktailActions";

const initialState = {
  cocktails: [],
  oneCocktail: null,
};

const cocktailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COCKTAIL_SUCCESS:
      return {...state, cocktails: action.cocktails};
    case FETCH_ONE_COCKTAIL_SUCCESS:
      return {...state, oneCocktail: action.coctails};
    default:
      return state;
  }
};

export default cocktailsReducer;
