import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";

import NewCocktail from "./containers/NewCocktail/NewCocktail";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Cocktail from "./containers/Cocktail/Cocktail";
import OneCocktail from "./containers/OneCocktail/OneCocktail";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props} /> : <Redirect to="/login" />
};

const Routes = ({user}) => {
  return (
    <Switch>
      <Route path="/" exact component={Cocktail}/>
      <Route path="/posts/:id" exact component={OneCocktail}/>
      <ProtectedRoute
        isAllowed={user && user.role}
        path="/cocktails/new"
        exact
        component={NewCocktail}
      />
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
    </Switch>
  );
};

export default Routes;