const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Cocktail = require('./models/Cocktail');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const user = await User.create(
      {
          username: 'Donna Alcgbfaeidhad Changsky',
          password: nanoid(),
          role: 'user',
          token: nanoid(),
          displayName: 'Donna',
          facebookId: '101764717742248',
          avatarImage: 'http://urlid.ru/bq2y'
      },

      {
          username: 'Tom Alcgadidfabhf Greenesen',
          password: nanoid(),
          role: 'user',
          token: nanoid(),
          displayName: 'Tom',
          facebookId: '104447104136938',
          avatarImage: 'http://urlid.ru/bq31'
      },
      {
          username: 'Carol Alcgjhjeeefaj Wisemanman',
          password: nanoid(),
          role: 'admin',
          token: nanoid(),
          displayName: 'moderaTER',
          facebookId: '106946100551408',
          avatarImage: 'http://urlid.ru/bq2w'
      },
  );

  await Cocktail.create(
    {
      user: user[0]._id,
      title: 'Мохито',
      image: 'mojito.jpg',
      recipe: 'Разрежьте лайм на 4 дольки. Положите листья мяты и 2 дольки лайма в стакан из толстого стекла. ' +
        'Подавите мяту и лайм мадлером или ложкой, чтобы они пустили сок. ' +
        'Добавьте ещё 1 дольку лайма и сахар, а после снова подавите.\n' +
        'Заполните стакан льдом почти до самого верха. Налейте ром на лёд и добавьте содовую. ' +
        'Аккуратно размешайте коктейльной ложкой и добавьте больше сахара, если это необходимо.' +
        ' Украсьте готовый коктейль долькой лайма и листиками мяты.',
      ingredient: [
        {title: 'лайма', amount: '0.5'},
        {title: 'свежих листьев мяты', amount: '10'},
        {title: 'чайные ложки коричневого сахара', amount: '2'},
        {title: 'дроблёного льда', amount: '200 g'},
        {title: 'белого рома', amount: '50 ml'},
        {title: 'содовой', amount: '100 ml'},
      ]
    },
    {
      user: user[0]._id,
      title: 'Дайкири',
      image: 'daikiri.jpg',
      recipe: 'Влейте ром, сок лайма и сахарный сироп в шейкер для коктейлей. ' +
        'Добавьте лёд, накройте крышкой и встряхните до охлаждения. Процедите в охлаждённый бокал.',
      ingredient: [
        {title: 'белого рома', amount: '60 ml'},
        {title: 'сока лайма', amount: '30 ml'},
        {title: 'чайная ложка сахарного сиропа', amount: '1'},
        {title: 'дроблёного льда', amount: '200 g'},
      ]
    },
    {
      user: user[0]._id,
      title: 'Манхэттен',
      image: 'manhattan.png',
      recipe: 'Влейте бурбон, вермут и ангостуру в шейкер для коктейлей и встряхните. ' +
        'Положите кубики льда в бокал, а после процедите в него смесь из шейкера. Украсьте коктейль вишенкой.',
      ingredient: [
        {title: 'бурбона', amount: '50 ml'},
        {title: 'красного вермута', amount: '25 ml'},
        {title: 'ангостуры', amount: '1 ml'},
        {title: 'льда в кубиках', amount: '200 g'},
        {title: 'коктейльная вишня', amount: '1'},
      ]
    },

  );

  return connection.close();
};


run().catch(error => {
  console.error('Something wrong happened...', error);
});
