const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');
const Cocktail = require('../models/Cocktail');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
  try {
    let criteria = {published: true};

    if (!req.user){
      criteria = {
        removed: false,
        $or:[
          {published: true}
        ]
      }
    }
    if (req.user && req.user.role === 'user'){
      criteria = {
        removed: false,
        $or:[
          {published: true},
          {user:req.user._id}
        ]
      }
    } else if (req.user && req.user.role === 'admin'){
      criteria = {}
    }

    const cocktails = await Cocktail.find(criteria).sort('name');

    return res.send(cocktails)
  }catch (e) {
    return res.status(500).send(e)
  }
});

router.get('/:id',tryAuth, async (req, res) => {
  try {
    const cocktail = await Cocktail.findById(req.params.id);
    res.send({cocktail});
  }catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', [auth, permit('user', 'admin')], upload.single('image'), (req, res) => {
  const cocktailData = req.body;

  if (req.file) {
    cocktailData.image = req.file.filename;
  }

  cocktailData.ingredient = JSON.parse(req.body.ingredient);
  cocktailData.user = req.user._id;

  const cocktail = new Cocktail(cocktailData);

  cocktail.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.post('/:id/toggle_published', [auth, permit('admin')], async (req, res) => {
  const cocktail = await Cocktail.findById(req.params.id);

  if (!cocktail) {
    return res.sendStatus(404);
  }

  cocktail.published = !cocktail.published;

  await cocktail.save();

  res.send(cocktail);
});

router.post('/:id/toggle_removed', [auth, permit('admin')], async (req, res) => {
  const cocktail = await Cocktail.findById(req.params.id);

  if (!cocktail) {
    return res.sendStatus(404);
  }

  cocktail.removed = !cocktail.removed;

  await cocktail.save();

  res.send(cocktail);
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
  const success = {message:'Removed'};
  try {
    const cocktail = await Cocktail.findById(req.params.id);

    await cocktail.remove();
    res.sendStatus(200)

  }catch(e)
  {
    res.sendStatus(400)
  }

  return res.send(success)
});


module.exports = router;