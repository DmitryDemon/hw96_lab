const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  dbUrl: 'mongodb://localhost/cocktail',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '329156731318325',
    appSecret: 'adb69d1ca110af66d509203d0a9c652d' // insecure!
  }
};
